import React from 'react';
import io from 'socket.io-client';

/**
 * Class Socket
 */
class Socket extends React.Component {
  /**
   * constructor of class Socket
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.socket = io('http://localhost:4000');
  }

  /**
   * mmmhh
   * @return {*}
   */
  getSocket() {
    return this.socket;
  }

  /**
   * emit
   * @param {Object} data
   */
  emitLogin(data) {
    this.socket.emit('USER_LOGIN_SEND', data);
  }
}

const SocketClient = new Socket();

export default SocketClient;
