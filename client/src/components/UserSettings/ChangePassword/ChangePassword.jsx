import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import './ChangePassword.scss';
import auth from '../../Security';

/**
 * Class ChangePassword
 */
class ChangePassword extends React.Component {
  /**
   * Constructor of class ChangePassword
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      currentPassword: '',
      newPassword: '',
      confirmNewPassword: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Function to change the value of the inputs
   * @param {Event} e
   */
  handleChange(e) {
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  }

  /**
   * @param {Event} e
   */
  handleSubmit(e) {
    e.preventDefault();
    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BACKEND_API}/users/password-change`,
      data: JSON.stringify({
        'userid': auth.getUser(),
        'currentPassword': this.state.currentPassword,
        'newPassword': this.state.confirmNewPassword,
      }),
    }).then((res) => {
      this.props.toggleChangePassword();
    }).catch((err) => {
      console.log('err');
    });
  }

  /**
   * Render of class ChangePassword
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="popup-user-settings-change-password-container">
        <div className="popup-user-settings-change-password-content d-flex justify-content-center align-items-center w-100 h-100">
          <form className="form form-change-password" onSubmit={this.handleSubmit}>
            <div className="m-5">
              <div className="form-header-container">
                <div className="form-header-cotent">
                  <div className="form-title-container">
                    <div className="form-title-container">
                      <h1 className="color-light text-uppercase form-title">Change your password</h1>
                    </div>
                  </div>
                </div>
              </div>

              <div className="form-group mt-6">
                <label className="text-uppercase ml-2 mb-3 color-light" htmlFor="password">Current Password <span className="form-required">*</span></label>
                <input required className='input form-control' placeholder="Current Password" onChange={this.handleChange} value={this.state.currentPassword} type="password" name="currentPassword" id="currentPassword"/>
              </div>

              <div className="form-group mt-6">
                <label className="text-uppercase ml-2 mb-3 color-light" htmlFor="password">New Password <span className="form-required">*</span></label>
                <input required className='input form-control' placeholder="New Password" onChange={this.handleChange} value={this.state.newPassword} type="password" name="newPassword" id="newPassword"/>
              </div>

              <div className="form-group mt-6">
                <label className="text-uppercase ml-2 mb-3 color-light" htmlFor="password">Confirm New Password <span className="form-required">*</span></label>
                <input required className='input form-control' placeholder="Confirm New Password" onChange={this.handleChange} value={this.state.confirmNewPassword} type="password" name="confirmNewPassword" id="confirmNewPassword"/>
              </div>

              <div className="d-flex flex-direction-row-reverse">
                <button className="mt-2 btn btn-primary mx-1" type="submit">Done</button>
                <button type="button" className="ml-auto mt-2 btn btn-primary mx-1" onClick={this.props.toggleChangePassword}>Chancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  toggleChangePassword: PropTypes.func,
};

export default ChangePassword;
