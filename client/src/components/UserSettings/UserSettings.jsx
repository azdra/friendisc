import React from 'react';
import './UserSettings.scss';
import Settings from './Settings/Settings';
import PropTypes from 'prop-types';

/**
 * Class UserSettings
 */
class UserSettings extends React.Component {
  /**
   * Constructor of class UserSettings
   * @param {Props} props
   */
  constructor(props) {
    super(props);
  }

  /**
   * Render of class UserSettings
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="popup-user-settings-container">
        <div className="popup-user-settings-content h-100vh max-vh-100">
          <div className="settings-user-left-container pt-12">
            <div className="settings-user-left-content mr-5">
              <div className="settings-user-left">
                <div className="settings-user-widget-container">
                  <div className="settings-user-widget-content mr-2">
                    <div className="settings-user-widget-user-settings">
                      <div className="settings-user-widget-title pb-1 text-uppercase color-gray-500">
                          User settings
                      </div>
                      <div className="settings-user-widget">
                        <div className="btn btn-settings w-100">My Account</div>
                      </div>
                    </div>

                    <div className="divider mx-auto my-3 bg-gray-500"/>

                    <div className="settings-user-widget-app-settings">
                      <div className="settings-user-widget-title pb-1 text-uppercase color-gray-500">
                          App Settings
                      </div>
                      <div className="settings-user-widget">
                        <div onClick={() => {
                          if (localStorage.getItem('theme') === 'light') {
                            localStorage.setItem('theme', 'dark');
                            document.getElementsByTagName('html')[0].setAttribute('theme', 'dark');
                          } else {
                            localStorage.setItem('theme', 'light');
                            document.getElementsByTagName('html')[0].setAttribute('theme', 'light');
                          }
                        }} className="btn btn-settings w-100">Appearance</div>
                        <div className="btn btn-settings w-100">Language</div>
                      </div>
                    </div>
                  </div>
                </div>
                <button onClick={this.props.toggleClose} className="btn btn-primary my-2">Close</button>
              </div>
            </div>
          </div>
          <div className="settings-user-right-container pt-12">
            <div className="settings-user-right-content ml-5">
              <div className="settings-user-right">
                <div className="settings-user-widget-container">
                  <div className="settings-user-widget-content">
                    <Settings/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserSettings.propTypes = {
  toggleClose: PropTypes.func,
};

export default UserSettings;
