import React from 'react';
import ChangePassword from '../ChangePassword/ChangePassword';

/**
 * Class Settings
 */
class Settings extends React.Component {
  /**
   * Constructor of class Settings
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      togglePassword: false,
    };

    this.toggleChangePassword = this.toggleChangePassword.bind(this);
  }

  /**
   * Enable/Disable Popup for Change password
   */
  toggleChangePassword() {
    this.setState({
      togglePassword: !this.state.togglePassword,
    });
  }

  /**
   * Render of class Settings
   * @return {JSX.Element}
   */
  render() {
    return (
      <div>
        <h1>My Account</h1>
        <div className="mt-5">
          <p>My Identification</p>
          <button className="mt-1 btn-copy" onClick={(e) => {
            navigator.clipboard.writeText(e.target.innerText).then((r) => console.log(r));
          }}>{window.$user.id}</button>
          <p className="text-small mt-1">Click to copy my Identification</p>
        </div>
        <div className="mt-5">
          <p>My Username</p>
          <p className="mt-1">{window.$user.username}</p>
        </div>
        <div className="mt-5">
          <p>My Email</p>
          <p className="mt-1">{window.$user.email}</p>
        </div>
        <div className="mt-5">
          <p>Change my password</p>
          <button onClick={this.toggleChangePassword} className="btn btn-primary mt-2">change password</button>
        </div>
        {this.state.togglePassword ? <ChangePassword toggleChangePassword={this.toggleChangePassword}/> : null}
      </div>
    );
  }
}

export default Settings;
