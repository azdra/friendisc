import React from 'react';
import './UserInfo.scss';
import UserIcon from '../Icons/User';
import GearIcon from '../Icons/Gear';
import UserSettings from '../UserSettings/UserSettings';
import propTypes from 'prop-types';

/**
 * Class UserInfo
 */
class UserInfo extends React.Component {
  /**
   * Constructor of class UserInfo
   * @param {Props} props
   */
  constructor(props) {
    super(props);
  }


  /**
   * Function for Open/Close the user settings
   * @param {Event} e
   */
  settingUser(e) {
    this.setState({
      openSettings: !this.state.openSettings,
    });
  }

  /**
   * Render of class UserInfo
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="user-info-content d-flex align-items-center h-100">
        <div className="user-info-left-container ml-2 mr-2">
          <div className="user-info-left-content">
            <div className="user-avatar-container">
              <div className={
                window?.$user.picture ? 'user-avatar-content d-flex justify-content-center align-items-center h-100 w-100 position-relative' :
                  'user-avatar-content d-flex justify-content-center align-items-center h-100 w-100 position-relative bg-primary'
              }>
                {
                  window?.$user.picture ? <img className="image-preview" src={process.env.REACT_APP_BACKEND+'/images/users/'+window?.$user.picture}/> : <UserIcon/>
                }
              </div>
            </div>
          </div>
        </div>
        <div className="user-info-middle-container">
          <div className="user-info-middle-content">
            <div className="user-username-container">
              <div className="user-username-content">
                <div className="user-username">{window?.$user.username}</div>
              </div>
            </div>
            <div className="my-1"/>
            <div className="user-status-container">
              <div className="user-status-content">
                <div className="user-status">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, reprehenderit!</div>
              </div>
            </div>
          </div>
        </div>
        <div className="user-info-right-container ml-auto">
          <div className="user-info-right-content">
            <div role="button" className="btn user-settings-container d-flex align-items-center justify-content-center">
              <div className="user-settings-content">
                <GearIcon/>
              </div>
              <div className="btn-click w-100 h-100">
                <div role="button" onClick={this.props.openSettings}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UserInfo.propTypes = {
  openSettings: propTypes.func,
};

export default UserInfo;
