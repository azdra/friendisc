import React from 'react';
import './ServerInfo.scss';
import PropTypes from 'prop-types';

/**
 * Class ServerInfo
 */
class ServerInfo extends React.Component {
  /**
   * Constructor of class ServerInfo
   * @param {Props} props
   */
  constructor(props) {
    super(props);
  }

  /**
   * Render of class ServerInfo
   * @return {JSX.Element}
   */
  render() {
    const {name} = this.props.dataName;
    return (
      <div className="server-info-content h-100 d-flex align-items-center px-5">
        <div className="server-info-left-container">
          <div className="server-info-left-content">
            <div className="server-info-name-container">
              <div className="server-info-name-content">
                <div className="server-info-name">
                  <h1>{name}</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ServerInfo.propTypes = {
  dataName: PropTypes.object,
};

export default ServerInfo;
