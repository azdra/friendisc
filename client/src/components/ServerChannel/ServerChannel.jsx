import React from 'react';
import PropsType from 'prop-types';
import HashtagIcon from '../Icons/Hashtag';
import axios from 'axios';
import auth from '../Security';

/**
 * Class ServerChannel
 */
class ServerChannel extends React.Component {
  /**
   * Constructor of class ServerChannel
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      selectChannel: null,
    };

    this.channelRef = [];
    this.channelClick = this.channelClick.bind(this);
    this.createInvite = this.createInvite.bind(this);
  }

  /** MESSAGE_SERVER_RECEIVE
   * Function for select channels
   * @param {Event} e
   * @param {boolean} load
   */
  channelClick(e, load = false) {
    // console.log(e);
    const target = e.target ?? e.children[2].firstChild;
    const parent = this.channelRef[target.getAttribute('data-key')];

    if (!this.state.selectChannel) {
      parent.classList.add('active');
      this.setState({
        selectChannel: parent,
      });
    } else {
      if (this.state.selectChannel?.classList.contains('active') && this.state.selectChannel !== parent) {
        this.state.selectChannel.classList.remove('active');
        this.setState({
          selectChannel: parent,
        });
        parent.classList.add('active');
      }
    }
    const data = {
      id: target.getAttribute('data-channel'),
      name: target.getAttribute('data-name'),
      topic: target.getAttribute('data-topic'),
    };
    // console.log('SERVER CHANNEL CLICK DATA SEND', data);
    this.props.channelClick(data, {first: load});
  }

  /**
   * Component to select channel when he is loaded
   */
  componentDidMount() {
    this.channelClick(this.channelRef[0], true);
  }

  /**
   * Component to select channel when he is updated
   * @param {Readonly<P>} prevProps
   * @param {Readonly<S>} prevState
   * @param {SS} snapshot
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.dataServer.id !== prevProps?.dataServer.id) {
      // console.log('Change Server');
      this.channelClick(this.channelRef[0], this.props.dataServer.id !== prevProps?.dataServer.id);
    }
  }

  /**
   * Function for create an invite
   * @param {Event} e
   */
  createInvite(e) {
    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BACKEND_API}/invites`,
      data: JSON.stringify({
        server: this.props.dataServer.id,
        owner: auth.getUser(),
      }),
    }).then((res) => {
      navigator.clipboard.writeText(res.data.code).then((r) => {
        e.target.innerText = 'Copied!';
        setTimeout(() => {
          e.target.innerText = 'Create invite';
        }, 2000);
      });
    });
  }

  /**
   * Render of class ServerChannel
   * @return {JSX.Element}
   */
  render() {
    const {dataServer} = this.props;
    return (
      <div className="channel-server-container">
        <div className="channel-server-content">
          <div className="m-2">
            <button onClick={this.createInvite} className="d-flex align-items-center w-100 btn btn-primary btn-channel btn-create-invite">Create invite</button>
          </div>
          {
            dataServer.channels.map((channel, i) => {
              return (
                <div key={i} className="channel-element position-relative">
                  <div role="button" tabIndex="0" className="d-flex align-items-center py-2 pl-5 w-100 btn btn-channel" ref={(ref) => this.channelRef[i] = ref}>
                    <div className="channel-type-container mr-2">
                      <div className="channel-type-content">
                        <div className="channel-type">
                          {
                              channel.type === 'text' ? <HashtagIcon/> : null
                          }
                        </div>
                      </div>
                    </div>
                    <div className="channel-name-container">
                      <div className="channel-name-content">
                        <div className="channel-name">
                          {channel.name}
                        </div>
                      </div>
                    </div>
                    <div className="btn-channel-click w-100 h-100">
                      <div role="button"
                        data-channel={channel.id}
                        data-name={channel.name}
                        data-topic={channel.topic}
                        data-key={i}
                        onClick={this.channelClick}
                      />
                    </div>
                    <div className="channel-widget-container">
                      <div className="channel-widget-content">
                        <div className="channel-invite-container">
                          <div className="channel-invite-content">
                            <div className="channel-invite">

                            </div>
                          </div>
                        </div>
                        <div className="channel-settings-container">
                          <div className="channel-settings-content">
                            <div className="channel-settings">

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="select-info position-absolute"/>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

ServerChannel.propTypes = {
  dataServer: PropsType.object,
  channelClick: PropsType.func,
};

export default ServerChannel;
