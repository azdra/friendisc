import React from 'react';
import {Trans} from 'react-i18next';
import RegisterFormStep1 from './Steps/Step1';
import formCheckValidity from '../../components/Utils/FormCheckValidity';
import './RegisterForm.scss';
import HiddenEyeIcon from '../Icons/HiddenEye';
import VisibleEyeIcon from '../Icons/VisibleEye';
import {Link, Redirect} from 'react-router-dom';
import auth from '../Security';
import RegisterFormStep2 from './Steps/Step2';
import RegisterFormStep3 from './Steps/Step3';
import axios from 'axios';

/**
 * Class RegisterForm
 */
class RegisterForm extends React.Component {
  /**
   * Constructor of class RegisterForm
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      confirmPassword: '',

      username: '',
      firstname: '',
      lastname: '',

      picture: '',

      currentStep: 1,
      maxStep: 3,
      redirect: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this._next = this._next.bind(this);
    this._back = this._back.bind(this);
    this.getAvatar = this.getAvatar.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Function to change the input value
   * @param {Event} e
   */
  handleChange(e) {
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  }

  /**
   * Nice
   * @param {Event} e
   */
  handleSubmit(e) {
    e.preventDefault();

    const formData = new FormData();
    formData.append('username', this.state.username);
    formData.append('password', this.state.password);
    formData.append('email', this.state.email);
    formData.append('firstname', this.state.firstname);
    formData.append('lastname', this.state.lastname);
    formData.append('picture', this.state.picture);

    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BACKEND_API}/users`,
      header: {
        'content-type': 'application/json',
      },
      data: formData,
    }).then((res) => {
      this.setState({
        redirect: true,
      });
    });
  }

  /**
   * machala
   * @param {Event} e
   * @private
   */
  _next(e) {
    const form = e.target;
    e.preventDefault();

    formCheckValidity(form).then((formElement) => {
      let currentStep = this.state.currentStep;
      currentStep = currentStep >= this.state.maxStep -1 ? this.state.maxStep: currentStep + 1;
      this.setState({
        currentStep: currentStep,
      });
    }).catch((formElement) => {
      console.log('bad', formElement);
    });
  }

  /**
   * machala
   * @private
   */
  _back() {
    let currentStep = this.state.currentStep;
    currentStep = currentStep <= 1? 1: currentStep - 1;
    this.setState({
      currentStep: currentStep,
    });
  }

  /**
   *
   * @param {File} avatar
   */
  getAvatar(avatar) {
    console.log('change avatar');
    this.setState({picture: avatar});
  }

  /**
   * Function to display the current step
   * @param {number} currentStep
   * @return {JSX.Element}
   */
  displayStep(currentStep) {
    switch (currentStep) {
      case 1:
        return (
          <RegisterFormStep1
            email={this.state.email}
            password={this.state.password}
            confirmPassword={this.state.confirmPassword}
            handleChange={this.handleChange}
            _next={this._next}
          />
        );
        break;
      case 2:
        return (
          <RegisterFormStep2
            _next={this._next}
            _back={this._back}
            handleChange={this.handleChange}
            username={this.state.username}
            firstname={this.state.firstname}
            lastname={this.state.lastname}
          />
        );
        break;
      case 3:
        return (
          <RegisterFormStep3
            _back={this._back}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            username={this.state.username}
            firstname={this.state.firstname}
            lastname={this.state.lastname}
            getAvatar={this.getAvatar}
          />
        );
        break;
    }
  }


  /**
   * Render of class RegisterForm
   * @return {JSX.Element}
   */
  render() {
    if (this.state.redirect) return <Redirect to="/login" />;
    return (
      <div className="register-container form form-login position-relative mx-auto">
        <div className="register-content m-5">
          <div className="form-header-container">
            <div className="form-header-cotent">
              <div className="form-title-container">
                <div className="form-title-content">
                  <h1 className="text-uppercase form-title">
                    <Trans>REGISTER_TITLE</Trans>
                  </h1>
                  <p className="mt-2 step">
                    Step: {this.state.currentStep} / {this.state.maxStep}
                  </p>
                </div>
              </div>
              <div className="form-subtitle-container mt-12">
                <div className="form-subtitle-content">
                  <h1 className="form-subtitle">
                    <Trans>REGISTER_SUBTITLE</Trans>
                  </h1>
                </div>
              </div>
            </div>
          </div>

          <div className="form-body-container">
            <div className="form-body-content">
              {
                this.displayStep(this.state.currentStep)
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterForm;
