import React from 'react';
import {Trans, withTranslation} from 'react-i18next';
import propTypes from 'prop-types';
import HiddenEyeIcon from '../../Icons/HiddenEye';
import VisibleEyeIcon from '../../Icons/VisibleEye';

/**
 * Class RegisterFormStep2
 */
class RegisterFormStep2 extends React.Component {
  /**
   * Render of class RegisterFormStep1
   * @return {JSX.Element}
   */
  render() {
    const {t} = this.props;
    return (
      <form onSubmit={this.props._next} noValidate>
        {/* USERNAME */}
        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="username">
              <Trans>USERNAME</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              required
              placeholder={t('TYPE_USERNAME')}
              className='input form-control'
              value={this.props.username}
              onChange={this.props.handleChange}
              type="text"
              name="username"
              id="username"/>
            <div className="invalid-feedback">
              <Trans>PROVIDE_USERNAME</Trans>
            </div>
          </div>
        </div>

        {/* firstname */}
        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="firstname">
              <Trans>FIRSTNAME</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              required
              placeholder={t('TYPE_FIRSTNAME')}
              className='input form-control'
              value={this.props.firstname}
              onChange={this.props.handleChange}
              type="text"
              name="firstname"
              id="firstname"/>
            <div className="invalid-feedback">
              <Trans>PROVIDE_FIRSTNAME</Trans>
            </div>
          </div>
        </div>

        {/* lastname */}
        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="lastname">
              <Trans>LASTNAME</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              required
              placeholder={t('TYPE_LASTNAME')}
              className='input form-control'
              value={this.props.lastname}
              onChange={this.props.handleChange}
              type="text"
              name="lastname"
              id="lastname"/>
            <div className="invalid-feedback">
              <Trans>PROVIDE_LASTNAME</Trans>
            </div>
          </div>
        </div>
        <div className="form-footer-container">
          <div className="form-footer-content">
            <div className="mt-8 d-flex">
              <button className="btn btn-primary w-100 py-4 m-1 py-4" onClick={this.props._back} type='button'>Back</button>
              <button className="btn btn-primary w-100 py-4 m-1 py-4" type='submit'>Next</button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

RegisterFormStep2.propTypes = {
  username: propTypes.string,
  firstname: propTypes.string,
  lastname: propTypes.string,
  handleChange: propTypes.func,
  _next: propTypes.func,
  _back: propTypes.func,
  t: propTypes.func,
};

export default withTranslation()(RegisterFormStep2);
