import React from 'react';
import {Trans} from 'react-i18next';
import propTypes from 'prop-types';
import HiddenEyeIcon from '../../Icons/HiddenEye';
import VisibleEyeIcon from '../../Icons/VisibleEye';
import Avatar from '../../Avatar/Avatar';

/**
 * Class RegisterFormStep3
 */
class RegisterFormStep3 extends React.Component {
  /**
   * Constructor of class RegisterFormStep3
   * @param {Props} props
   */
  constructor(props) {
    super(props);
  }

  /**
   * Render of class RegisterFormStep3
   * @return {JSX.Element}
   */
  render() {
    return (
      <form onSubmit={this.props.handleSubmit} noValidate>
        <div className='form-group mt-8'>
          <Avatar getAvatar={this.props.getAvatar}/>
        </div>
        <div className="form-footer-container">
          <div className="form-footer-content">
            <div className="d-flex mt-8">
              <button className="btn btn-primary w-100 py-4 m-1 py-4" onClick={this.props._back} type='button'>Back</button>
              <button className="btn btn-primary w-100 py-4 m-1 py-4" type='submit'>Done!</button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

RegisterFormStep3.propTypes = {
  handleChange: propTypes.func,
  handleSubmit: propTypes.func,
  _back: propTypes.func,
  getAvatar: propTypes.func,
};

export default RegisterFormStep3;
