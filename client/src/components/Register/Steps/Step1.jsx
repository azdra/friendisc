import React from 'react';
import {Trans, withTranslation} from 'react-i18next';
import propTypes from 'prop-types';
import HiddenEyeIcon from '../../Icons/HiddenEye';
import VisibleEyeIcon from '../../Icons/VisibleEye';

/**
 * Class RegisterFormStep1
 */
class RegisterFormStep1 extends React.Component {
  /**
   * Constructor of class RegisterFormStep1
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      passwordShow: false,
      passwordMatch: true,
    };

    this.togglePasswordShow = this.togglePasswordShow.bind(this);
    this.checkPassword = this.checkPassword.bind(this);
  }

  /**
   * Function to change the type of input
   */
  togglePasswordShow() {
    this.setState({
      passwordShow: !this.state.passwordShow,
    });
  }

  /**
   * @param {Event} e
   */
  checkPassword(e) {
    e.preventDefault();
    if (this.props.password !== this.props.confirmPassword) {
      this.setState({
        passwordMatch: false,
      });
      return;
    }
    this.props._next(e);
  }

  /**
   * Render of class RegisterFormStep1
   * @return {JSX.Element}
   */
  render() {
    const {t} = this.props;
    return (
      <form onSubmit={this.checkPassword} noValidate>
        {/* EMAIL ADDRESS */}
        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="email">
              <Trans>EMAIL</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              placeholder={t('TYPE_EMAIL')}
              required
              className='input form-control'
              value={this.props.email}
              onChange={this.props.handleChange}
              type="email"
              name="email"
              id="email"/>
            <div className="invalid-feedback">
              <Trans>PROVIDE_EMAIL</Trans>
            </div>
          </div>
        </div>

        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="password">
              <Trans>PASSWORD</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              placeholder={t('TYPE_PASSWORD')}
              minLength={6}
              required={true}
              className='input form-control'
              onChange={this.props.handleChange}
              value={this.props.password}
              type={this.state.passwordShow ? 'text': 'password'}
              name="password"
              id="password"/>
            <button onClick={this.togglePasswordShow} type="button" className="btn input-group-text input-group-left">
              {
                this.state.passwordShow ? <HiddenEyeIcon/> : <VisibleEyeIcon/>
              }
            </button>
            <div className="invalid-feedback">
              <Trans>PROVIDE_PASSWORD</Trans>
            </div>
          </div>
        </div>

        <div className="form-group mt-6">
          <div className="input-group">
            <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="confirmPassword">
              <Trans>CONFIRM_PASSWORD</Trans>
              <span className="form-required ml-1">*</span>
            </label>
            <input
              placeholder={t('TYPE_CONFIRM_PASSWORD')}
              minLength={6}
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck="false"
              required={true}
              className='input form-control'
              onChange={this.props.handleChange}
              value={this.props.confirmPassword}
              type={this.state.passwordShow ? 'text': 'password'}
              name="confirmPassword"
              id="confirmPassword"/>
            <button onClick={this.togglePasswordShow} type="button" className="btn input-group-text input-group-left">
              {
                this.state.passwordShow ? <HiddenEyeIcon/> : <VisibleEyeIcon/>
              }
            </button>
            <div className="invalid-feedback">
              <Trans>PROVIDE_PASSWORD</Trans>
            </div>
          </div>
        </div>

        <div className="form-footer-container">
          <div className="form-footer-content">
            <div className="mt-6">
              {!this.state.passwordMatch ? <p className="password-match text-center my-1">
                <Trans>PASSWORD_DONT_MATCH</Trans>
              </p> : null}
              <button className="btn btn-primary w-100 py-4" type='submit'>Next</button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

RegisterFormStep1.propTypes = {
  email: propTypes.string,
  password: propTypes.string,
  confirmPassword: propTypes.string,
  handleChange: propTypes.func,
  _next: propTypes.func,
  t: propTypes.func,
};

export default withTranslation()(RegisterFormStep1);
