import React from 'react';
import axios from 'axios';
import {withTranslation} from 'react-i18next';
import PropType from 'prop-types';
import AddServerPopup from '../AddServerPopup/AddServerPopup';
// import ServerSideSkeleton from './ServerSideSkeleton';

import HomeIcon from '../Icons/Home';
import PlusIcon from '../Icons/Plus';

import './ServerSide.scss';
import io from 'socket.io-client';

/**
 * Class ServerSide
 */
class ServerSide extends React.Component {
  /**
   * Constructor of class ServerSide
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      servers: [],
      selectServer: null,
      popupAddServer: false,
    };

    // this.socket = io('http://localhost:4000');

    this.serverRef = [];
    this.homeRef = React.createRef();

    /* this.socket.on('ADD_SERVER_CREATE_RECEIVE', (dataServer) => {
      this.addServer(dataServer);
    });*/

    this.props.socket.getSocket().on('ADD_SERVER_CREATE_RECEIVE', (dataServer) => {
      const merge = Object.assign({}, dataServer.server, {
        channels: [
          dataServer.channel,
        ],
      });
      this.addServer(merge);
    });

    this.props.socket.getSocket().on('ADD_SERVER_JOIN_RECEIVE', (dataServer) => {
      this.addServer(dataServer.server);
    });

    this.selectServer = this.selectServer.bind(this);
    this.tooltip = this.tooltip.bind(this);
    this.goToHome = this.goToHome.bind(this);
    this.togglePopup = this.togglePopup.bind(this);
    this.joinServer = this.joinServer.bind(this);
    this.createServer = this.createServer.bind(this);
    this.addServer = this.addServer.bind(this);
  }

  /**
   * ghsdighdsiuo
   * @param {Object} data
   */
  createServer(data) {
    this.props.socket.getSocket().emit('ADD_SERVER_CREATE_SEND', data);
    // this.socket.emit('ADD_SERVER_CREATE_SEND', dataServer);
  }

  /**
   * ghsdighdsiuo
   * @param {Object} dataJoin
   */
  joinServer(dataJoin) {
    this.props.socket.getSocket().emit('ADD_SERVER_JOIN_SEND', dataJoin);
  }

  /**
   * fqifjsqio
   * @param {Object} dataServer
   */
  addServer(dataServer) {
    // console.log(dataServer);
    this.setState((state, props) => {
      return {
        servers: [...state.servers, dataServer],
      };
    });
  }

  /**
   * Enable/Disable the popup
   */
  togglePopup() {
    this.setState({
      popupAddServer: !this.state.popupAddServer,
    });
  }

  /**
   * Recovers all the user's servers
   */
  componentDidMount() {
    this.setState({selectServer: this.homeRef.current});
    this.homeRef.current.classList.add('select');

    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BACKEND_API}/server/join`,
      data: JSON.stringify({
        user_id: sessionStorage.getItem('user_id_cache'),
      }),
    }).then((res) => {
      this.setState({
        servers: res.data,
      });
      this.props.loadHome('loadServers');
    }).catch((err) => console.error(err));
  }

  /**
   * Function for select servers
   * @param {Event} e
   */
  selectServer(e) {
    const {target} = e;
    const parent = this.serverRef[target.getAttribute('data-key')];

    if (!this.state.selectServer) {
      parent.classList.add('select');
      this.setState({
        selectServer: parent,
      });
    } else {
      if (this.state.selectServer?.classList.contains('select') && this.state.selectServer !== parent) {
        this.state.selectServer.classList.remove('select');
        this.setState({
          selectServer: parent,
        });
        parent.classList.add('select');
      }
    }
    this.props.setClickServer({
      id: target.getAttribute('data-id'),
      name: target.getAttribute('data-name'),
      channels: this.state.servers[target.getAttribute('data-key')].channels,
    });
  }

  /**
   * Function to display the tooltip
   * @param {Event} e
   */
  tooltip(e) {
    const tooltip = document.createElement('div');
    tooltip.classList.add('tooltip');
    tooltip.innerText= e.target.dataset.tooltip;
    tooltip.style.left = e.target.getBoundingClientRect().right + 20 + 'px';
    tooltip.style.top = e.target.getBoundingClientRect().top + (e.target.getBoundingClientRect().height/4.5) + 'px';
    document.body.append(tooltip);

    e.target.addEventListener('mouseleave', () => {
      tooltip.remove();
    });
  }

  /**
   * Function to return to the home menu
   * @param {Event} e
   */
  goToHome(e) {
    if (this.state.selectServer && this.state.selectServer.classList.contains('select')) this.state.selectServer.classList.remove('select');
    this.setState({
      selectServer: this.homeRef.current,
    });
    this.homeRef.current.classList.add('select');
    this.props.goToHome();
  }

  /**
   * Render of class ServerSide
   * @return {JSX.Element}
   */
  render() {
    const {t} = this.props;
    return (
      <div className="section-server-side-content">
        {this.state.popupAddServer ? <AddServerPopup joinServer={this.joinServer} createServer={this.createServer} closePopup={this.togglePopup}/> : null}
        <div className="my-2">
          <div role="button" tabIndex="0" className="btn btn-server btn-home" ref={this.homeRef}>
            <div className="btn-icon-container d-flex justify-content-center align-items-center h-100">
              <HomeIcon/>
            </div>
            <div className="btn-click position-absolute">
              <div data-tooltip={t('HOME')} onMouseEnter={this.tooltip} onClick={this.goToHome}/>
            </div>
          </div>

          {this.state.servers.length >=1 ? <div className="separator mx-auto my-2"/> : null}

          <div className="server-list-container">
            <div className="server-list-content">
              {/* {this.state.servers.length <= 0 ? <ServerSideSkeleton/> : null}*/}
              {this.state.servers.map((server, i) => {
                const uriPicture = `${process.env.REACT_APP_BACKEND}/images/servers/${server.picture}`;
                return (
                  <div key={i} className="server-element position-relative" ref={(ref) => (this.serverRef[i] = ref)}>
                    <div className="select-info"/>
                    <div role="button" tabIndex="0" className={server.picture ? 'btn btn-server' : 'btn btn-server btn-server-no-icon'}>
                      <div className="btn-icon-container d-flex justify-content-center align-items-center h-100">
                        {
                          server.picture ? <img className="image-preview" src={uriPicture}/> : <h1 className="color-primary">{server.name[0]}</h1>
                        }
                      </div>
                      <div className="btn-click w-100 h-100">
                        <div role="button"
                          data-tooltip={server.name}
                          data-id={server.id}
                          data-name={server.name}
                          data-key={i}
                          onMouseEnter={this.tooltip}
                          onClick={this.selectServer}/>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>

          <div className="separator mx-auto my-2"/>

          <div role="button" tabIndex="0" className="btn btn-server btn-create-server">
            <div className="btn-icon-container d-flex justify-content-center align-items-center h-100">
              <PlusIcon/>
            </div>
            <div className="btn-click position-absolute">
              <div data-tooltip={t('ADD_SERVER')} onClick={this.togglePopup} onMouseEnter={this.tooltip}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ServerSide.propTypes = {
  goToHome: PropType.func,
  setClickServer: PropType.func,
  t: PropType.func,
  loadHome: PropType.func,
  socket: PropType.any,
};

export default withTranslation()(ServerSide);
