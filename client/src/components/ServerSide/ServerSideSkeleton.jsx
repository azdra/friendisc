import React from 'react';

/**
 * Class ServerSideSkeleton
 */
class ServerSideSkeleton extends React.Component {
  /**
   * Render of class ServerSideSkeleton
   * @return {JSX.Element}
   */
  render() {
    return (
      <div>
        {
          [...Array(Math.floor(Math.random() * 10) + 5)].map((x, i) => {
            return (
              <div key={i} className="server-element">
                <div role="button" tabIndex="0" className="btn btn-server servers-button-skeleton">
                  <div className="btn-icon-container d-flex justify-content-center align-items-center h-100">
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }
}

export default ServerSideSkeleton;
