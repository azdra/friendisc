import React from 'react';

/**
 * Class SendIcon
 */
export default class SendIcon extends React.Component {
  /**
   * Render of class SendIcon
   * @return {JSX.Element}
   */
  render() {
    return (
      <svg className="icon-send icon-svg" viewBox="0 0 479.058 479.058">
        <path fill="currentColor" fillRule="evenodd" clipRule="evenodd"
          d="M481.508 210.336L68.414 38.926c-17.403-7.222-37.064-4.045-51.309 8.287S-3.098 78.551 1.558 96.808L38.327 241h180.026c8.284 0 15.001 6.716 15.001 15.001s-6.716 15.001-15.001 15.001H38.327L1.558 415.193c-4.656 18.258 1.301 37.262 15.547 49.595 14.274 12.357 33.937 15.495 51.31 8.287l413.094-171.409C500.317 293.862 512 276.364 512 256.001s-11.683-37.862-30.492-45.665z"
        />
      </svg>
    );
  }
}
