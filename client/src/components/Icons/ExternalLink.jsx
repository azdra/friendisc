import React from 'react';

/**
 * Class ExternalLinkIcon
 */
export default class ExternalLinkIcon extends React.Component {
  /**
   * Render of class ExternalLinkIcon
   * @return {JSX.Element}
   */
  render() {
    return (
      <svg className="icon-externallink icon-svg" viewBox="0 0 20 20">
        <path fill="currentColor" fillRule="evenodd" clipRule="evenodd" d="M17 17H3V3h5V1H3a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-5h-2z"/>
        <path fill="currentColor" fillRule="evenodd" clipRule="evenodd" d="M19 1h-8l3.29 3.29-5.73 5.73 1.42 1.42 5.73-5.73L19 9V1z"/>
      </svg>
    );
  }
}
