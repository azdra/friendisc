import React from 'react';

/**
 * Class MessageIcon
 */
export default class MessageIcon extends React.Component {
  /**
   * Render of class MessageIcon
   * @return {JSX.Element}
   */
  render() {
    return (
      <svg className="icon-message icon-svg" viewBox="0 0 477.867 477.867">
        <path fill="currentColor" fillRule="evenodd" clipRule="evenodd"
          d="M426.667.002H51.2C22.923.002 0 22.925 0 51.202v273.067c0 28.277 22.923 51.2 51.2 51.2h60.587l-9.284 83.456a17.07 17.07 0 0 0 28.382 14.558l108.919-98.014h186.863c28.277 0 51.2-22.923 51.2-51.2V51.202c0-28.277-22.923-51.2-51.2-51.2z"
        />
      </svg>
    );
  }
}
