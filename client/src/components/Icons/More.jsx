import React from 'react';

/**
 * Class MoreIcon
 */
export default class MoreIcon extends React.Component {
  /**
   * Render of class MoreIcon
   * @return {JSX.Element}
   */
  render() {
    return (
      <svg className="icon-more icon-svg" viewBox="0 0 375.636 375.635">
        <path fill="currentColor" fillRule="evenodd" clipRule="evenodd"
          d="M41.013 228.825C18.396 228.825 0 210.438 0 187.818c0-22.608 18.396-41.007 41.013-41.007s41.013 18.398 41.013 41.007c-.001 22.62-18.396 41.007-41.013 41.007zm144.5 0c-22.617 0-41.013-18.387-41.013-41.007 0-22.608 18.396-41.007 41.013-41.007s41.013 18.398 41.013 41.007c-.001 22.62-18.4 41.007-41.013 41.007zm149.11 0c-22.613 0-41.013-18.387-41.013-41.007 0-22.608 18.399-41.007 41.013-41.007s41.013 18.398 41.013 41.007c0 22.62-18.399 41.007-41.013 41.007z"
          transform="matrix(0 -1 1 0 0 375.635986)"
        />
      </svg>
    );
  }
}
