import React from 'react';

/**
 * ChatSkeleton
 */
class ChatSkeleton extends React.Component {
  /**
   * Render of class ChatSkeleton
   * @return {JSX.Element}
   */
  render() {
    return (
      <div>
        {
          [...Array(Math.floor(Math.random() * 25) + 10)].map((m, i) => {
            return (
              <div key={i} className={'message-container py-1 px-5 mt-3'}>
                <div className="message-content d-flex">
                  <div className="message-user-profile-container mr-4">
                    {
                      <div className="message-user-profile-content">
                        <div className="message-user-profile d-flex justify-content-center align-items-center h-100 w-100 position-relative chat-picture-skeleton">
                        </div>
                      </div>
                    }
                  </div>
                  <div className="message-info-container">
                    <div className="message-info-content">
                      {
                        <div className="message-header d-flex">
                          <div className="message-author-container mb-2">
                            <div className="message-author-content">
                              <div className="message-author">
                                <h5 className="color-primary d-inline-block">
                                  <div className="chat-username-skeleton" style={{width: Math.floor(Math.random() * 200) + 100}}> </div>
                                </h5>
                              </div>
                            </div>
                          </div>
                          <div className="message-send-at-container">
                            <div className="message-send-at-content">
                              <div className="message-send-at ml-2">
                                <div className="chat-time-skeleton"> </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      }
                      <div className="message-content-container">
                        <div className="message-content-content">
                          <div className="chat-content-skeleton" style={{width: Math.floor(Math.random() * 500) + 20}}> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }
}

export default ChatSkeleton;
