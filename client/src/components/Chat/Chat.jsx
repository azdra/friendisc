import React from 'react';
import HashtagIcon from '../Icons/Hashtag';
import UserIcon from '../Icons/User';
import BurgerIcon from '../Icons/Burger';
import SendIcon from '../Icons/Send';
import RenderText from '../Utils/RenderText';
import io from 'socket.io-client';
import fetchMessages from '../Utils/FetchMessages';
import sendMessage from '../Utils/SendMessage';
import dayjs from 'dayjs';
import yesterday from 'dayjs/plugin/isYesterday';
import today from 'dayjs/plugin/isToday';
import PropTypes from 'prop-types';
import './Chat.scss';
import ChatSkeleton from './ChatSkeleton';
import Notify from '../Utils/Notification';


dayjs.extend(yesterday);
dayjs.extend(today);


/**
 * Class Chat
 */
class Chat extends React.Component {
  /**
   * Constructor of class Chat
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      messages: {},
      message: '',
    };

    this.chatScrollRef = React.createRef();
    this.inputMessageRef = React.createRef();

    this.props.socket.getSocket().on('MESSAGE_SERVER_RECEIVE', (newMessage) => {
      this.receivedMessage(newMessage);
    });

    this.receivedMessage = this.receivedMessage.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * wave
   * @param {e} e
   */
  handleSubmit(e) {
    e?.preventDefault();
    const message = this.state.message.trim();
    if (!message || (message === 0)) return;
    this.setState({
      message: '',
    });

    sendMessage({content: message, channel: this.props.channel.id}).then((res) => {
      this.props.socket.getSocket().emit('MESSAGE_SERVER_SEND', {
        message: res,
        channel: this.props.channel.id,
      });
    });
  }

  /**
   * Salut
   * @param {Event} e
   */
  handleChange(e) {
    this.setState({
      message: e.target.value,
    });
  }

  /**
   * Component to load messages when the component is updated
   * @param {Readonly<P>} prevProps
   * @param {Readonly<S>} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.props.channel !== prevProps.channel) {
      if (!this.state.messages[this.props.channel.id] || this.state.messages[this.props.channel.id].length >= 0) {
        fetchMessages(this.props.channel.id).then((res) => {
          this.setState((state, props) => {
            return {
              messages: {
                ...state.messages,
                [this.props.channel.id]: res,
              },
            };
          });
        });
      }
    }
  }

  /**
   * Component to load messages when the component is loaded
   */
  componentDidMount() {
    fetchMessages(this.props.channel.id).then((res) => {
      this.setState({
        messages: {
          [this.props.channel.id]: res,
        },
      });
      this.chatScrollRef.current ? this.chatScrollRef.current.scrollTop = this.chatScrollRef.current.scrollHeight : null;
    });
  }

  /**
   * Function to receive messages
   * @param {Object} data
   */
  receivedMessage(data) {
    if (this.state.messages[data.channel]) {
      this.setState((state, props) => {
        return {
          messages: {
            ...state.messages,
            [data.channel]: this.state.messages[data.channel] ? [...state.messages[data.channel], data.message] : [],
          },
          message: '',
        };
      });
      const chatScrollRef = this?.chatScrollRef?.current;
      chatScrollRef?.scrollTop ? chatScrollRef.scrollTop = chatScrollRef?.scrollHeight : null;
    }
  }

  /**
   * Render of class Chat
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="section-chat-side-content">

        <div className="channel-info-container">
          <div className="channel-info-content d-flex h-100 align-items-center">
            <div className="d-flex align-items-center ml-2">
              <div className="mr-2 btn-burger d-block d-md-none">
                <button onClick={this.props.chatMode} className="btn">
                  <BurgerIcon/>
                </button>
              </div>
              <div className="channel-type-container mr-2 mt-1">
                <div className="channel-type-content">
                  <div className="channel-type">
                    <HashtagIcon/>
                  </div>
                </div>
              </div>
              <div className="channel-name-container">
                <div className="channel-name-content">
                  <div className="channel-name">
                    {this.props.channel.name}
                  </div>
                </div>
              </div>
            </div>
            {
              this.props.channel.topic ? <div>
                <div className="separator-vertical d-none d-md-block"/>
                <div className="channel-topic-container d-none d-md-block">
                  <div className="channel-topic-content">
                    <div className="channel-topic">
                      {this.props.channel.topic}
                    </div>
                  </div>
                </div>
              </div> : null
            }
          </div>
        </div>

        <div className="d-flex flex-direction-column chat-container">
          <div className="chat-content scroll" ref={this.chatScrollRef}>
            <div className="messages-wrapper-container">
              <div className="messages-wrapper-content">
                {
                  !this.state.messages[this.props.channel.id] ?
                    <ChatSkeleton/> : null
                }
                {
                  this.state.messages[this.props.channel.id]?.map((message, i) => {
                    const splitWords = message.content.split(' ');
                    const lastMessage = this.state.messages[this.props.channel.id][i-1];
                    const lastMessageAt = dayjs(lastMessage?.create_at);
                    const lastMessageSplit = lastMessageAt.format('YYMMDD HHmm').split(' ');

                    const createAt = dayjs(message?.create_at);
                    const createMessageSplit = createAt.format('YYMMDD HHmm').split(' ');
                    const dateOfSendMessage = createAt.format('h:mm A');
                    const yesterday = createAt.isYesterday();
                    const today = createAt.isToday();
                    const sendInterval = ((lastMessageSplit[0] === createMessageSplit[0]) && ((createMessageSplit[1] - lastMessageSplit[1]) <= 20));

                    const groupedMessage = ((lastMessage?.owner?.id === message.owner.id) && sendInterval);
                    return (
                      <div className={groupedMessage ? 'message-container py-1 px-5' : 'message-container py-1 px-5 mt-3'} key={i}>
                        <div className="message-content d-flex">
                          <div className="message-user-profile-container mr-4">
                            {
                              groupedMessage ?
                                <div className="text-time color-gray-500 pt-1">
                                  {dateOfSendMessage}
                                </div> :
                                <div className="message-user-profile-content">
                                  <div className="message-user-profile d-flex justify-content-center align-items-center h-100 w-100 position-relative default">
                                    {
                                      message.owner.user.picture ? <img className="image-preview" src={process.env.REACT_APP_BACKEND+'/images/users/'+message.owner.user.picture}/> : <UserIcon/>
                                    }
                                  </div>
                                </div>
                            }
                          </div>
                          <div className="message-info-container">
                            <div className="message-info-content">
                              {
                                groupedMessage ? null : <div className="message-header d-flex">
                                  <div className="message-author-container mb-2">
                                    <div className="message-author-content">
                                      <div className="message-author">
                                        <h5 className="d-inline-block">
                                          {message.owner?.nickname}
                                        </h5>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="message-send-at-container">
                                    <div className="message-send-at-content">
                                      <div className="message-send-at">
                                        <time className="ml-3 text-tiny color-gray-500" dateTime={createAt.format('DD-MM-YYYY')}>
                                          {(today || yesterday) ? (today ? 'Today' : 'Yesterday') + ' at ' + dateOfSendMessage: createAt.format('DD-MM-YYYY')}
                                        </time>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              }
                              <div className="message-content-container">
                                <div className="message-content-content">
                                  {
                                    <RenderText text={message.content}/>
                                  }
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })
                }
                <div className="space"/>
              </div>
            </div>
          </div>

          <form className="form form-message pt-1" method="POST" action="" onSubmit={this.handleSubmit}>
            <div className="form-content">
              <div className="form-group mx-5 mb-5 position-relative">
                <input type="text" autoComplete="off" className={this.state.message ? 'form-control input input-message pr-12' : 'form-control input input-message'} placeholder={'Message #'+this.props.channel.name} onChange={this.handleChange} value={this.state.message} name="message" id="message"/>
                {
                  this.state.message ?
                    <button className="position-absolute btn-send-message btn d-block d-sm-none">
                      <SendIcon/>
                    </button> : null
                }
              </div>
            </div>
          </form>

        </div>
      </div>
    );
  }
}

Chat.propTypes = {
  dataServer: PropTypes.object,
  channel: PropTypes.object,
  chatMode: PropTypes.func,
  socket: PropTypes.any,
};

export default Chat;
