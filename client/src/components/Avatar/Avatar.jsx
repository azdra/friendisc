import React from 'react';
import propTypes from 'prop-types';
import CameraIcon from '../Icons/Camera';
import {Trans} from 'react-i18next';

/**
 * Class Avatar
 */
class Avatar extends React.Component {
  /**
   * Constructor of class Avatar
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      imagePreview: '',
      acceptedFormat: [
        '.png',
        '.jpeg',
        '.jpg',
        '.gif',
      ],
      badImage: false,
    };

    this.imagePreview = React.createRef();

    this.handleChangeImage = this.handleChangeImage.bind(this);
    this.clickAddImage = this.clickAddImage.bind(this);
  }

  /**
   * Function
   */
  clickAddImage() {
    this.imagePreview.current.click();
  }

  /**
   *
   * @param {Event} e
   * @return {*}
   */
  handleChangeImage(e) {
    if (e.target.files[0]) {
      const img = e.target.files[0];
      const preview = URL.createObjectURL(img);
      if (img.size > ((2*1000)*1000)) {
        this.setState({
          badImage: true,
        });
      } else {
        this.setState({
          imagePreview: preview,
          badImage: false,
        });
        return this.props.getAvatar(img);
      }
    }
  }

  /**
   * Render of class Avatar
   * @return {JSX.Element}
   */
  render() {
    const formats = this.state.acceptedFormat;
    return (
      <div className="avatar-container">
        <div className="w-100 picture-server d-block mx-auto" >
          { this.state.imagePreview ? <img src={this.state.imagePreview} className="image-preview" alt=""/> : null }
          <button type='button' onClick={this.clickAddImage} className="btn image-preview-camera-container w-100 h-100">
            { !this.state.imagePreview ? <div className="image-preview-camera"> <CameraIcon/> </div> : null }
          </button>
          <input
            className="btn d-none"
            accept={this.state.acceptedFormat}
            size={2000}
            type="file"
            onChange={this.handleChangeImage}
            ref={this.imagePreview}
            name="picture"/>
        </div>

        {
          this.state.badImage ? <div className="d-block w-100 my-3 invalid text-center">
            <Trans>IMAGE_TOO_BIG</Trans>
          </div> : null
        }

        <label htmlFor="chooseimage" className="d-block text-center mt-3">
          <Trans>AVATAR_ADD_SUBTITLE</Trans>
        </label>
        <div className="file-info text-center">
          <div>
            <Trans>AVATAR_ADD_LIMIT_SIZE</Trans>
          </div>
          <div className="mt-1">
            <Trans i18nKey="AVATAR_ADD_ACCEPTED_FORMAT">{{formats}}</Trans>
          </div>
        </div>
      </div>
    );
  }
}

Avatar.propTypes = {
  picture: propTypes.string,
  getAvatar: propTypes.func,
};

export default Avatar;
