import React from 'react';
import axios from 'axios';
import {Redirect} from 'react-router-dom';
import io from 'socket.io-client';

/**
 * Class Auth
 */
class Auth extends React.Component {
  /**
   * fqsfnhusi
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.login = this.login.bind(this);
  }

  /**
   * Auth Login
   * @param {string} email
   * @param {string} password
   * @return {Promise}
   */
  login(email, password) {
    return new Promise(((resolve, reject) => {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/user/login`,
        header: {
          'content-type': 'application/json',
        },
        data: {
          'email': email,
          'password': password,
        },
      }).then((res) => {
        const {id, picture, username, email} = res.data;
        window.$user = {id, picture, username, email};
        sessionStorage.setItem('user_id_cache', id);
        resolve(true);
      }).catch((err) => reject(err));
    }));
  }

  /**
   * Get if user is connected
   * @return {JSX.Element}
   */
  getConnect() {
    if ( !(sessionStorage.getItem('user_id_cache') || sessionStorage.getItem('email_cache')) ) {
      return <Redirect to='/login' />;
    }
  }

  /**
   * Get user ID
   * @return {string}
   */
  getUser() {
    return sessionStorage.getItem('user_id_cache');
  }
}

const auth = new Auth();

export default auth;
