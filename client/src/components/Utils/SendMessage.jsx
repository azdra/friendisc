import axios from 'axios';
import auth from '../Security';

/**
 * Function to send the message
 * @param {string} content
 * @param {string|number} channel
 * @return {Promise<unknown>}
 */
const sendMessage = ({content, channel}) => {
  return new Promise((resolve, reject) => {
    try {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/messages`,
        header: {
          'content-type': 'application/json',
        },
        data: JSON.stringify({
          'content': content,
          'owner': auth.getUser(),
          'channel': channel,
        }),
      }).then((res) => resolve(res.data))
          .catch((e) => reject(e));
    } catch (e) {
      return reject(e);
    }
  });
};

export default sendMessage;
