import React from 'react';
import PropTypes from 'prop-types';
import {regexURI} from './tools';
import io from 'socket.io-client';
import {getLinkPreview, getPreviewFromContent} from 'link-preview-js';
/**
 * Class RenderText
 */
class RenderText extends React.Component {
  /**
   * Constructor of class RenderText
   * @param {Props} props
   */
  constructor(props) {
    super(props);
  }

  /**
   * Function to render the text
   * @return {*}
   */
  renderText() {
    const parts = this.props.text.split(' '); // re is a matching regular expression
    parts.map((words, i) => {
      if (regexURI.test(words)) {
        parts[i] = <div key={i} >
          <a className="message-content-link" href={words} target='_blank' rel="noreferrer">{words+' '}</a>
          <div className="link-preview-container">
            {/* <RenderUrl link={words}/>*/}
          </div>
        </div>;
      } else parts[i] = words+' ';
    });
    return parts;
  }

  /**
   * Render of class RenderText
   * @return {JSX.Element}
   */
  render() {
    const text = this.renderText();
    return (
      <div>
        <div className="some_text_class">{text}</div>
      </div>

    );
  }
}

export default RenderText;

RenderText.propTypes = {
  text: PropTypes.string,
};
