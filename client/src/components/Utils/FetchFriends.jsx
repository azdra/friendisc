import axios from 'axios';

/**
 * Function to retrieve a user's friend list
 * @param {string} userId
 * @return {Promise<Friends>} FriendsList
 */
const fetchFriends = async (userId) => {
  return new Promise((resolve, reject) => {
    try {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/friends/${userId}`,
      }).then((res) => resolve(res.data))
          .catch((err) => reject(err));
    } catch (e) {
      reject(e);
    }
  });
};

export default fetchFriends;
