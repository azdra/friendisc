import React from 'react';
import PropTypes from 'prop-types';
import {io} from 'socket.io-client';
/**
 * Class RenderUrl
 */
class RenderUrl extends React.Component {
  /**
   * Constructor of class RenderUrl
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      data: {},
    };
  }
  /**
   * ....
   */
  componentDidMount() {
    this.socket.emit('LINK_MESSAGE_SEND', this.props.link);
  }

  /**
   * ....
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="link-preview-content">
        {this.props.link}
      </div>
    );
  }
}

export default RenderUrl;

RenderUrl.propTypes = {
  link: PropTypes.string,
};
