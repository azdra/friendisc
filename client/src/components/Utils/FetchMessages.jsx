import axios from 'axios';

/**
 * Function to retrieve messages from a channel
 * @param {string} ChannelId
 * @return {Promise<any>} Messages
 */
const fetchMessage = async (ChannelId) => {
  return new Promise((resolve, reject) => {
    try {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/messages/channel`,
        data: JSON.stringify({
          maxMessages: 50,
          channelId: ChannelId,
        }),
      }).then((res) => resolve(res.data.reverse()))
          .catch((err) => reject(err));
    } catch (e) {
      reject(e);
    }
  });
};

export default fetchMessage;
