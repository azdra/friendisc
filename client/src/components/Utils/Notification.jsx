/**
 * Class Notify
 */
class Notify {
  /**
   * Constructor of class Notify
   * @param {Object} data
   */
  constructor(data) {
    if (!('Notification' in window)) {
      return null;
    } else if (Notification.permission === 'granted') {
      new Notification('', data);
    } else if (Notification.permission !== 'denied') {
      Notification.requestPermission().then((permission) => {
        if (permission === 'granted') {
          new Notification('', data);
        }
      });
    }
  }
}

export default Notify;

