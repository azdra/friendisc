/**
 * @param {HTMLFormElement} form
 * @constructor
 */
const formCheckValidity = (form) => {
  return new Promise((resolve, reject) => {
    if (!form.checkValidity()) {
      const elm = [];
      for (const element of form.elements) {
        if (!element.checkValidity()) {
          element.parentNode?.classList?.add('was-unvalidated');
          elm.push(element);
        }
      }
      const error = {
        error: 'The form is not valid',
        element: elm,
      };
      reject(error);
    } else {
      for (const element of form.elements) {
        if (element.checkValidity()) {
          element.parentNode.classList.remove('was-unvalidated');
        }
      }
      resolve();
    }
  });
};

export default formCheckValidity;
