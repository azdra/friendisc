import React from 'react';
import PropTypes from 'prop-types';
import './AddServerPopup.scss';
import CreateServer from './CreateServer/CreateServer';
import JoinServer from './JoinServer/JoinServer';

/**
 * Class AddServerPopup
 */
class AddServerPopup extends React.Component {
  /**
   * Constructor of class AddServerPopup
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      mode: 'create',
    };

    this.changeMode = this.changeMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /**
   * Function to change the
   */
  changeMode() {
    if (this.state.mode === 'create') this.setState({mode: 'join'});
    else this.setState({mode: 'create'});
  }

  /**
   * Function for change state
   * @param {SyntheticEvent} e
   */
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  /**
   * Render of class AddServerPopup
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className='popup-server-create d-flex justify-content-center align-items-center w-100 min-vh-100 position-fixed'>
        <div className="position-relative form form-server-create">
          <button type="button" onClick={this.props.closePopup} className="btn color-primary d-inline-block popup-close position-absolute d-flex justify-content-center align-items-center">X</button>
          {
            this.state.mode === 'create' ?
              <CreateServer closePopup={this.props.closePopup} createServer={this.props.createServer} changeMode={this.changeMode}/> :
              <JoinServer closePopup={this.props.closePopup} joinServer={this.props.joinServer} changeMode={this.changeMode}/>
          }
        </div>
      </div>
    );
  }
}

AddServerPopup.propTypes = {
  closePopup: PropTypes.func,
  createServer: PropTypes.func,
  joinServer: PropTypes.func,
};

export default AddServerPopup;
