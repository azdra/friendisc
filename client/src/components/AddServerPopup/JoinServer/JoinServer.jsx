import React from 'react';
import {Trans, withTranslation} from 'react-i18next';
import Avatar from '../../Avatar/Avatar';
import formCheckValidity from '../../Utils/FormCheckValidity';
import auth from '../../Security';
import axios from 'axios';
import propTypes from 'prop-types';
import CreateServer from '../CreateServer/CreateServer';

/**
 * CLass JoinServer
 */
class JoinServer extends React.Component {
  /**
   * Constructor of class JoinServer
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      inviteCode: '',
      invalidCode: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
   * Function for change state
   * @param {Event} e
   */
  handleChange(e) {
    this.setState({
      inviteCode: e.target.value,
    });
  }

  /**
   * Function for change state
   * @param {Event} e
   */
  handleSubmit(e) {
    const form = e.target;

    formCheckValidity(form).then((formElement) => {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/invites/join`,
        data: JSON.stringify({
          code: this.state.inviteCode,
          user: auth.getUser(),
        }),
      }).then((res) => {
        this.props.closePopup();
        this.props.joinServer(res.data);
      }).catch((err) => {
        if (err.response.data.code === 'inviteInvalidOrExpired') {
          this.setState({
            invalidCode: 'INVALID_INVITE_CODE',
          });
        } else if (err.response.data.code === 'alreadyInTheServer') {
          this.setState({
            invalidCode: 'ALREADY_JOIN_SERVER',
          });
        }
      });
    }).catch((formElement) => {
      console.log('bad');
      // console.log(formElement);
    });
    e.preventDefault();
  }

  /**
   * Render of class JoinServer
   * @return {JSX.Element}
   */
  render() {
    const {t} = this.props;
    return (
      <form onSubmit={this.handleSubmit} noValidate>
        <div className="m-5">
          <div className='form-title d-inline-block'>
            <h1 className='form-title-text text-uppercase'>
              <Trans>SERVER_JOIN_TITLE</Trans>
            </h1>
          </div>

          <div className='form-group mt-8'>
            <div className="input-group">
              <label className='text-uppercase ml-2 mb-3 w-100 d-block' htmlFor='inviteCode'>
                <Trans>INVITE_CODE</Trans>
              </label>
              <input
                placeholder={t('TYPE_INVITE_CODE')}
                autoFocus={true}
                onChange={this.handleChange}
                value={this.state.inviteCode}
                className='form-control input'
                type="text" name="inviteCode"
                id="inviteCode"
                required
              />
              <div className="invalid-feedback">
                <Trans>PROVIDE_INVITE_CODE</Trans>
              </div>
              {
                this.state.invalidCode ? <div className="invalid-feedback d-block">
                  <Trans>{this.state.invalidCode}</Trans>
                </div> : null
              }
            </div>
          </div>

          <div className='form-group mt-5'>
            <button className='btn btn-primary mx-auto w-100 text-tiny' type='submit'>
              <Trans>JOIN</Trans>
            </button>
          </div>

          <div className='form-group mt-2'>
            <button className='btn btn-primary mx-auto w-100 text-tiny' type='button'>
              <Trans>ADD_SERVER</Trans>
            </button>
          </div>
        </div>
      </form>
    );
  }
}

JoinServer.propTypes = {
  closePopup: propTypes.func,
  joinServer: propTypes.func,
  changeMode: propTypes.func,
  t: propTypes.func,
};

export default withTranslation()(JoinServer);
