import React from 'react';
import Avatar from '../../Avatar/Avatar';
import propTypes from 'prop-types';
import auth from '../../Security';
import {Trans, withTranslation} from 'react-i18next';
import formCheckValidity from '../../Utils/FormCheckValidity';
import axios from 'axios';

/**
 * CLass CreateServer
 */
class CreateServer extends React.Component {
  /**
   * Constructor of class CreateServer
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      servername: '',
      picture: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.getAvatar = this.getAvatar.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  /**
   * Function to get the avatar
   * @param {File} avatar
   */
  getAvatar(avatar) {
    this.setState({picture: avatar});
  }

  /**
   * Function for submit the forms
   * @param {SyntheticEvent} e
   */
  handleSubmit(e) {
    const form = e.target;
    e.preventDefault();

    formCheckValidity(form).then((formElement) => {
      const formData = new FormData();
      formData.append('picture', this.state.picture);
      formData.append('name', this.state.servername);
      formData.append('owner', auth.getUser());

      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/servers`,
        header: {
          'content-type': 'multipart/form-data',
        },
        data: formData,
      }).then((res) => {
        this.props.closePopup();
        this.props.createServer(res.data);
      });
    }).catch((formElement) => {
      // console.log(formElement);
    });
  }

  /**
   * Function for change state
   * @param {SyntheticEvent} e
   */
  handleChange(e) {
    this.setState({servername: e.target.value});
  }

  /**
   * Render of class CreateServer
   * @return {JSX.Element}
   */
  render() {
    const {t} = this.props;
    return (
      <form onSubmit={this.handleSubmit} encType="multipart/form-data" noValidate>
        <div className="m-5">
          <div className='form-title d-inline-block'>
            <h1 className='form-title-text text-uppercase'>
              <Trans>SERVER_CREATE_TITLE</Trans>
            </h1>
          </div>

          <div className='form-group mt-8'>
            <Avatar getAvatar={this.getAvatar}/>
          </div>

          <div className='form-group mt-8'>
            <div className="input-group">
              <label className='text-uppercase ml-2 mb-3 w-100 d-block' htmlFor='email'><Trans>SERVER_NAME</Trans></label>
              <input
                placeholder={t('TYPE_SERVER_NAME')}
                autoFocus={true}
                onChange={this.handleChange}
                value={this.state.servername}
                className='form-control input'
                type="text" name="servername"
                id="servername"
                required
              />
              <div className="invalid-feedback">
                <Trans>PROVIDE_SERVER_NAME</Trans>
              </div>
            </div>
          </div>

          <div className='form-group mt-5'>
            <button className='btn btn-primary mx-auto w-100 text-tiny' type='submit'>
              <Trans>ADD_SERVER</Trans>
            </button>
          </div>

          <div className='form-group mt-2'>
            <button className='btn btn-primary mx-auto w-100 text-tiny' onClick={this.props.changeMode} type='button'>
              <Trans>JOIN_SERVER</Trans>
            </button>
          </div>
        </div>
      </form>
    );
  }
}

CreateServer.propTypes = {
  closePopup: propTypes.func,
  createServer: propTypes.func,
  changeMode: propTypes.func,
  t: propTypes.func,
};


export default withTranslation()(CreateServer);
