import React from 'react';
import UserIcon from '../Icons/User';
import MessageIcon from '../Icons/Message';
import MoreIcon from '../Icons/More';

/**
 * Class FriendsSkeleton
 */
class FriendsSkeleton extends React.Component {
  /**
   * Render of class FriendsSkeleton
   * @return {JSX.Element}
   */
  render() {
    return (
      <div>
        {
          [...Array(Math.floor(Math.random() * 10) + 5)].map((x, i) => {
            return (
              <div key={i} className="friend-container p-1 py-1 mt-3">
                <div className="friend-content d-flex">
                  <div className="friend-picture-container mr-4 friend-picture-skeleton">
                    <div className="friend-picture-content">
                      <div className="friend-picture">
                      </div>
                    </div>
                  </div>
                  <div className="friend-info-container">
                    <div className="friend-info-content">
                      <div className="friend-username-container">
                        <div className="friend-username-content color-primary mb-1">
                          <div className="friend-username-skeleton"> </div>
                        </div>
                      </div>
                      <div className="friend-status-container">
                        <div className="friend-status-content">
                          <div className="friend-status mt-1">
                            <div className="friend-status-skeleton"> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="friend-widget-container mr-2">
                    <div className="friend-widget-content h-100 d-flex align-items-center">
                      <div className="friend-widget-message-container m-1">
                        <div className="friend-widget-message-content friend-widget-btn friend-button-skeleton">
                        </div>
                      </div>
                      <div className="friend-widget-more-container m-1">
                        <div className="friend-widget-more-content friend-widget-btn friend-button-skeleton">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="separator w-100 mt-2"/>
              </div>
            );
          })
        }
      </div>
    );
  }
}

export default FriendsSkeleton;
