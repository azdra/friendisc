import React from 'react';
import './Friends.scss';
import FriendsIcon from '../Icons/Friends';
import fetchFriends from '../Utils/FetchFriends';
import MessageIcon from '../Icons/Message';
import MoreIcon from '../Icons/More';
import UserIcon from '../Icons/User';
import PropTypes from 'prop-types';
import BurgerIcon from '../Icons/Burger';
// import FriendsSkeleton from './FriendsSkeleton';
import auth from '../Security';
import io from 'socket.io-client';
import axios from 'axios';

/**
 * Class Friends
 */
class Friends extends React.Component {
  /**
   * Constructor of class Friends
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      friends: [],
      filterMode: 'online',
      filterParams: {
        'all': {
          title: 'All Friends',
          func: function(friend) {
            return friend;
          },
        },
        'online': {
          title: 'Online',
          func: function(friend) {
            return friend.logged === true;
          },
        },
        'pending': {
          title: 'Pending',
          func: function(friend) {
            return friend;
          },
        },
        'blocked': {
          title: 'Blocked',
          func: function(friend) {
            return friend;
          },
        },
      },
      filterSelect: null,
    };

    this.onlineRef = React.createRef();

    this.props.socket.getSocket().on('GET_FRIENDS_LOGIN_LIST_RECEIVE', (friends) => {
      this.setState({
        friends,
      });
    });

    this.props.socket.getSocket().on('FRIENDS_LIST_DISCONNECT_SEND', (friend) => {
      const element = this.state.friends.findIndex((elm) => elm.id === friend.id);
      if (element >= 0) {
        this.state.friends[element].logged = false;
        this.setState({
          friends: this.state.friends,
        });
      }
    });

    this.props.socket.getSocket().on('FRIENDS_LIST_CONNECT_SEND', (friend) => {
      const element = this.state.friends.findIndex((elm) => elm.id === friend.id);
      if (element >= 0) {
        this.state.friends[element].logged = true;
        this.setState({
          friends: this.state.friends,
        });
      }
    });

    this.props.socket.getSocket().on('FRIENDS_SEND_REQUEST_RECEIVE', (friend) => {
      this.setState({
        friends: [friend],
      });
    });

    this.selectMode = this.selectMode.bind(this);
    this.addFriend = this.addFriend.bind(this);
    this.privateMessage = this.privateMessage.bind(this);
  }

  /**
   * Component to load friends when the component is loaded
   * @return {Promise<void>}
   */
  async componentDidMount() {
    this.setState({filterSelect: this.onlineRef.current});
    this.onlineRef.current.classList.add('active');
    fetchFriends(auth.getUser()).then((results) => {
      /* this.setState({
        friends: results,
      });*/
      this.props.loadHome('loadFriends');
      this.props.socket.getSocket().emit('GET_FRIENDS_LOGIN_LIST_SEND', results);
    });
  }

  /**
   * Select mode
   * @param {Event} e
   */
  selectMode(e) {
    if (this.state.filterSelect) {
      if (this.state.filterSelect.classList.contains('active')) {
        this.state.filterSelect.classList.remove('active');
      }
    }
    this.setState({filterSelect: e.target});
    e.target.classList.add('active');
    this.setState({
      filterMode: e.target.getAttribute('data-type'),
    });
  }

  /**
   * Function for adding your best friend
   */
  addFriend() {
    // 6771066962530271232
    const friendId = prompt('Enter the user id');
    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BACKEND_API}/friends`,
      data: JSON.stringify({
        friendId: friendId,
        userId: auth.getUser(),
      }),
    }).then((res) => {
      this.props.socket.getSocket().emit('FRIENDS_SEND_REQUEST_SEND', {
        friend: res.data,
        userId: auth.getUser(),
      });
    });
  }

  /**
   * Salyut
   * @param {Event} e
   */
  privateMessage(e) {
    console.log(e.target);
  }

  /**
   * Render of class Friends
   * @return {JSX.Element}
   */
  render() {
    const friendFilter = this.state.friends.filter((friend) => this.state.filterParams[this.state.filterMode].func(friend));
    return (
      <div className="section-friends-side-content">
        <div className="friends-header-container">
          <div className="friends-header-content d-flex align-items-center h-100">
            <div className="friends-header-title-container">
              <div className="friends-header-title-content d-flex align-items-center ml-2">
                <div className="btn-burger d-block d-md-none">
                  <button onClick={this.props.chatMode} className="btn">
                    <BurgerIcon/>
                  </button>
                </div>
                <div className="ml-2 friends-header-icon-container">
                  <div className="friends-header-icon-content">
                    <div className="friends-header-icon">
                      <FriendsIcon/>
                    </div>
                  </div>
                </div>
                <div className="friends-header-title ml-4">
                  <h1>Friends</h1>
                </div>
              </div>
            </div>
            <div className="separator-vertical d-none d-md-block"/>
            <div className="d-none d-md-block">
              <button data-type="online" ref={this.onlineRef} onClick={this.selectMode} className="btn btn-primary d-inline-block mx-1 btn-friend">Online</button>
              <button data-type="all" onClick={this.selectMode} className="btn btn-primary d-inline-block mx-1 btn-friend">All</button>
              <button data-type="all" className="btn btn-primary d-inline-block mx-1 btn-friend color-gray-500">Pending</button>
              <button data-type="all" className="btn btn-primary d-inline-block mx-1 btn-friend color-gray-500">Blocked</button>
              <button onClick={this.addFriend} className="btn btn-primary d-inline-block mx-1 btn-add-friend">Add Friend</button>
            </div>
          </div>
        </div>
        <div className="friends-list-container scroll">
          <div className="friends-list-content mx-10">
            <div className="friends-title-container">
              <div className="friends-title-content">
                <div className="friends-title mb-3 mt-5">
                  <h1 className="text-uppercase friend-size">{this.state.filterParams[this.state.filterMode].title} - {friendFilter.length}</h1>
                </div>
              </div>
            </div>

            <div className="separator w-100"/>

            <div className="friends-list">
              {/* {this.state.friends.length <= 0 ? <FriendsSkeleton/> : null}*/}
              {
                friendFilter.map((friend, i) => {
                  return (
                    <div key={i} className="friend-container p-1 py-1 mt-3">
                      <div className="friend-content d-flex">
                        <div className="friend-picture-container mr-4 default">
                          <div className={friend.logged ? 'user-status logged' : 'user-status no-logged'}/>
                          <div className="friend-picture-content">
                            <div className="friend-picture">
                              {
                                friend.picture ? <img className="image-preview" src={process.env.REACT_APP_BACKEND+'/images/users/'+friend.picture}/> : <UserIcon/>
                              }
                            </div>
                          </div>
                        </div>
                        <div className="friend-info-container">
                          <div className="friend-info-content">
                            <div className="friend-username-container">
                              <div className="friend-username-content mb-1">
                                <h1>{friend.username}</h1>
                              </div>
                            </div>
                            <div className="friend-status-container">
                              <div className="friend-status-content">
                                <div className="friend-status mt-1">
                                  <p className="text-muted text-small">
                                    {
                                      friend.logged ? friend.status ?? 'Online' : 'Offline'
                                    }
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="friend-widget-container mr-2">
                          <div className="friend-widget-content h-100 d-flex align-items-center">
                            <div className="friend-widget-message-container m-1">
                              <div className="friend-widget-message-content friend-widget-btn-content position-relative">
                                <MessageIcon/>
                                <div onClick={this.privateMessage} className="position-absolute friend-widget-btn"/>
                              </div>
                            </div>
                            <div className="friend-widget-more-container m-1">
                              <div className="friend-widget-more-content friend-widget-btn-content position-relative">
                                <MoreIcon/>
                                <div onClick={this.privateMessage} className="position-absolute friend-widget-btn"/>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="separator w-100 mt-2"/>
                    </div>
                  );
                })
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Friends.propTypes = {
  chatMode: PropTypes.func,
  loadHome: PropTypes.func,
  socket: PropTypes.any,
};

export default Friends;
