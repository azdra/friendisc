import React from 'react';
import HashtagIcon from '../Icons/Hashtag';
import FriendsIcon from '../Icons/Friends';

/**
 * Class PrivateChannel
 */
class PrivateChannel extends React.Component {
  /**
   * Constructor of class PrivateChannel
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.friendsRef = React.createRef();

    this.state = {
      selectChannel: null,
    };
  }

  /**
   *
   */
  componentDidMount() {
    this.setState({selectChannel: this.friendsRef.current});
    this.friendsRef.current.classList.add('active');
  }

  /**
   * Render of class PrivateChannel
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="private-channel-container">
        <div className="private-channel-content">
          <div className="channel-element position-relative m-2">
            <div role="button" tabIndex="0" className="d-flex align-items-center py-2 pl-5 w-100 btn btn-channel" ref={this.friendsRef}>
              <div className="channel-type-container mr-2">
                <div className="channel-type-content">
                  <div className="channel-type">
                    <FriendsIcon/>
                  </div>
                </div>
              </div>
              <div className="channel-name-container">
                <div className="channel-name-content">
                  <div className="channel-name">
                    Friends
                  </div>
                </div>
              </div>
              <div className="btn-channel-click w-100 h-100">
                <div role="button"

                />
              </div>
            </div>
          </div>

          <div className="mt-3 text-small m-2 color-gray-500">
            Direct Message

            {
              [...Array(7)].map((x, i) => {
                return (
                  <div key={i} className="channel-element-muted position-relative">
                    <div className="d-flex align-items-center py-2 pl-5 w-100 btn btn-channel">
                      <div className="channel-type-container mr-2">
                        <div className="channel-type-content">
                          <div className="channel-type">
                            <FriendsIcon/>
                          </div>
                        </div>
                      </div>
                      <div className="channel-name-container">
                        <div className="channel-name-content">
                          <div className="channel-name">
                            User {i}
                          </div>
                        </div>
                      </div>
                      <div className="btn-channel-click w-100 h-100">
                        <div role="button"

                        />
                      </div>
                    </div>
                  </div>
                );
              })
            }

          </div>
        </div>
      </div>
    );
  }
}

export default PrivateChannel;
