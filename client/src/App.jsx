import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';

import Home from './containers/Home/Home';
import SignIn from './containers/SignIn/SignIn';
import SignUp from './containers/SignUp/SignUp';
import NotFound from './containers/404/NotFound';

document.getElementsByTagName('html')[0].setAttribute('theme', localStorage.getItem('theme') ?? 'light');

/**
 * Class Application
 */
class Application extends React.Component {
  /**
   * Render of class Application
   * @return {JSX.Element}
   */
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/register" component={SignUp}/>
          <Route exact path="/login" component={SignIn}/>
          <Route exact path="/" component={Home}/>
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Application;
