import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

const validLang = ['en', 'fr'];
const resources = {};
for (const resource of validLang) {
  Object.assign(resources, {
    [resource]: {
      translation: require(`./assets/translations/${resource}.json`),
    },
  });
}

i18n
    .use(initReactI18next)
    .init({
      resources,
      lng: localStorage.getItem('lang') ?? 'en',

      keySeparator: false,

      interpolation: {
        escapeValue: false,
      },
    });

export default i18n;
