import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import auth from '../../components/Security';
import SocketClient from '../../components/Socket/Socket';
import formCheckValidity from '../../components/Utils/FormCheckValidity';
import {Trans, withTranslation} from 'react-i18next';
import propTypes from 'prop-types';
import HiddenEyeIcon from '../../components/Icons/HiddenEye';
import VisibleEyeIcon from '../../components/Icons/VisibleEye';

/**
 * Class SignIn
 */
class SignIn extends React.Component {
  /**
   * Constructor of class SignIn
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      badPassword: false,
      passwordVisible: false,
      redirectHome: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.togglePasswordType = this.togglePasswordType.bind(this);
  }

  /**
   * Function for change input value
   * @param {Event} e
   */
  handleChange(e) {
    const {name, value} = e.target;
    this.setState({
      [name]: value,
    });
  }

  /**
   * Function to Hide/Visible the password
   */
  togglePasswordType() {
    this.setState({
      passwordVisible: !this.state.passwordVisible,
    });
  }

  /**
   * Function for change input value
   * @param {Event} e
   */
  handleSubmit(e) {
    const form = e.target;
    e.preventDefault();

    formCheckValidity(form).then((formElement) => {
      const {email, password} = this.state;
      auth.login(email, password).then(() => {
        this.setState({redirectHome: true});
      }).catch((err) => {
        this.setState({badPassword: true});
      });
    }).catch((formElement) => {
      /* for (const formElementElement of formElement.element) {
        console.log(formElementElement);
        this.setState({
          [formElementElement.name]: '',
        });
      }*/
    });
  }

  /**
   * Render of class SignIn
   * @return {JSX.Element}
   */
  render() {
    if (this.state.redirectHome) return <Redirect to='/' />;
    const {t} = this.props;
    return (
      <div className="container login-page-container bg-texture">
        <div className="login-page-content d-flex justify-content-center align-items-center w-100 min-vh-100">
          <form className="login-container form form-login position-relative mx-auto" onSubmit={this.handleSubmit} noValidate>
            <div className="login-content m-5">
              <div className="form-header-container">
                <div className="form-header-cotent">
                  <div className="form-title-container">
                    <div className="form-title-container">
                      <h1 className="text-uppercase form-title">
                        <Trans>LOGIN_TITLE</Trans>
                      </h1>
                    </div>
                  </div>
                  <div className="form-subtitle-container mt-12">
                    <div className="form-subtitle-content">
                      <h1 className="form-subtitle">
                        <Trans>LOGIN_SUBTITLE</Trans>
                      </h1>
                    </div>
                  </div>
                </div>
              </div>

              <div className="form-body-container">
                <div className="form-body-content">
                  <div className="form-group mt-8">
                    <div className="input-group">
                      <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="email">
                        <Trans>EMAIL</Trans>
                      </label>
                      <input required className={this.state.badPassword ? 'input form-control border-danger' : 'input form-control'} placeholder={t('TYPE_EMAIL')} onChange={this.handleChange} value={this.state.email} type="email" name="email" id="email"/>

                      <div className="invalid-feedback">
                        <Trans>PROVIDE_EMAIL</Trans>
                      </div>
                    </div>
                  </div>

                  <div className="form-group mt-8">
                    <div className="input-group">
                      <label className="text-uppercase ml-2 mb-3 d-block w-100" htmlFor="password">
                        <Trans>PASSWORD</Trans>
                      </label>
                      <input required className={this.state.badPassword ? 'input form-control border-danger' : 'input form-control'} placeholder={t('TYPE_PASSWORD')} onChange={this.handleChange} value={this.state.password} type={this.state.passwordVisible ? 'text': 'password'} name="password" id="password"/>
                      <button onClick={this.togglePasswordType} type="button" className="btn input-group-text input-group-left">
                        {
                          this.state.passwordVisible ? <HiddenEyeIcon/> : <VisibleEyeIcon/>
                        }
                      </button>
                      <div className="invalid-feedback">
                        <Trans>PROVIDE_PASSWORD</Trans>
                      </div>
                    </div>
                  </div>

                  <div className="form-group mt-6">
                    <input type="checkbox" name="keepLogin" id="keepLogin"/>
                    <label className="ml-2" htmlFor="keepLogin">
                      <Trans>KEEP_LOGIN</Trans>
                    </label>
                  </div>
                </div>
              </div>

              <p className={this.state.badPassword ? 'd-block text-center my-3 color-danger' : 'd-none'}>
                <Trans>WRONG_PASSWORD_OR_WRONG_EMAIL</Trans>
              </p>
              <div className="form-footer-container">
                <div className="form-footer-content">
                  <div className="form-group mt-3">
                    <input className='btn btn-primary py-4 mx-auto w-100' type="submit" value="Log In"/>
                  </div>

                  <div className="form-group mt-4">
                    <div className="text-center">
                      <span className="d-block mb-2 not-account">
                        <Trans>NOT_ACCOUNT</Trans>
                      </span>
                      <div className="not-account-register-now">
                        <Link to='/register'>
                          <Trans>REGISTER_NOW</Trans>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

SignIn.propTypes = {
  t: propTypes.func,
};

export default withTranslation()(SignIn);
