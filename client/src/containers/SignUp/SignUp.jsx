import React from 'react';
import RegisterForm from '../../components/Register/RegisterForm';

/**
 * Class SignUp
 */
class SignUp extends React.Component {
  /**
   * Render of class SignUp
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className='container register-page-container bg-texture'>
        <div className='register-page-content d-flex justify-content-center align-items-center w-100 min-vh-100'>
          <RegisterForm/>
        </div>
      </div>
    );
  }
}

export default SignUp;
