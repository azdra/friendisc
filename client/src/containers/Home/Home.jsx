import React from 'react';
import ServerSide from '../../components/ServerSide/ServerSide';
import Chat from '../../components/Chat/Chat';
import PrivateChannel from '../../components/PrivateChannel/PrivateChannel';
import ServerChannel from '../../components/ServerChannel/ServerChannel';
import UserInfo from '../../components/UserInfo/UserInfo';
import ServerInfo from '../../components/ServerInfo/ServerInfo';
import Friends from '../../components/Friends/Friends';
import {Redirect} from 'react-router-dom';
import getUserAgent from '../../components/Utils/GetUserAgent';
import './Home.scss';
import auth from '../../components/Security';
import axios from 'axios';
import io from 'socket.io-client';
import SocketClient from '../../components/Socket/Socket';
import UserSettings from '../../components/UserSettings/UserSettings';

/**
 * Class Home
 */
class Home extends React.Component {
  /**
   * Constructor of class Home
   * @param {Props} props
   */
  constructor(props) {
    super(props);

    this.state = {
      dataChannel: {},
      channel: {},
      home: true,
      loadingAccount: false,
      redirectLogin: false,
      chatMode: false,

      loadHome: false,
      loadFriends: false,
      loadServers: false,

      openSettings: false,
    };

    this.chatRef = React.createRef();
    this.socket = SocketClient;

    this.getServerChannel = this.getServerChannel.bind(this);
    this.channelClick = this.channelClick.bind(this);
    this.goToHome = this.goToHome.bind(this);
    this.chatMode = this.chatMode.bind(this);
    this.loadHome = this.loadHome.bind(this);
    this.settingUser = this.settingUser.bind(this);
  }

  /**
   * gqdsg
   */
  componentDidMount() {
    const cache = sessionStorage.getItem('user_id_cache');
    try {
      axios({
        method: 'POST',
        url: `${process.env.REACT_APP_BACKEND_API}/users/${cache}`,
        header: {
          'content-type': 'application/json',
        },
      }).then((res) => {
        const {id, username, picture, email} = res.data;
        window.$user = {id, username, picture, email};
        this.socket.getSocket().emit('USER_LOGIN_SEND', res.data);
        this.setState({
          loadingAccount: true,
        });
      }).catch((err) => {
        this.setState({
          redirectLogin: true,
        });
      });
    } catch (e) {
      this.setState({
        redirectLogin: true,
      });
    }
  }

  /**
   * Get the channels of server's
   * @param {Object} dataServer
   */
  getServerChannel(dataServer) {
    this.setState({
      home: false,
      dataServer: dataServer,
    });
  }

  /**
   * Function to return to the home menu
   */
  goToHome() {
    this.setState({
      home: true,
    });
  }

  /**
   * Recover the channel click
   * @param {Object} channel
   * @param {Object} options
   */
  channelClick(channel, options) {
    this.setState({
      channel: channel,
    });

    if (!options.first) this.chatMode();
  }

  /**
   * gfdqsuifgqsuiod
   */
  chatMode() {
    if (getUserAgent()) {
      if (this.chatRef.current.classList.contains('full')) {
        this.chatRef.current.classList.remove('full');
      } else if (!this.chatRef.current.classList.contains('full')) {
        this.chatRef.current.classList.add('full');
      }
    }
  }

  /**
   * fqfqs
   * @param {string} name
   */
  loadHome(name) {
    this.setState({
      [name]: true,
    });

    if (this.state.loadFriends && this.state.loadServers) {
      this.setState({
        loadHome: true,
      });
    }
  }

  /**
   * Function for Open/Close the user settings
   * @param {Event} e
   */
  settingUser(e) {
    this.setState({
      openSettings: !this.state.openSettings,
    });
  }

  /**
   * Render of class NotFound
   * @return {JSX.Element}
   */
  render() {
    return (
      <div className="home-page-container">
        {this.state.redirectLogin ? <Redirect to="/login"/> : null}
        {
          !this.state.loadHome ? <div className="home-loading-container">
            <h1 className="text-center my-2">Welcome to Friendisc</h1>
            <h1 className="text-center">Loading the application...</h1>
            <div className="loading-content my-5">
              <div className="loading-element"/>
              <div className="loading-element"/>
              <div className="loading-element"/>
            </div>
            <div className="text-small text-center">
              <p className="my-1">Lets go to play <em>stone sheet scissors</em></p>
              <p className="my-1">Are you READY ?????</p>
              <p className="my-1">3.. 2.. 1..</p>
              <p className="my-1">oh fu**. You won</p>
              <p className="my-1">You can ENTER</p>
            </div>
          </div> : null
        }
        {
          this.state.loadingAccount ?
            <div className="home-page-content d-flex" ref={this.chatRef}>
              <section className="section-server-side-container position-relative min-vh-100 w-100">
                <ServerSide socket={this.socket} loadHome={this.loadHome} goToHome={this.goToHome} setClickServer={this.getServerChannel}/>
              </section>
              <section className="section-channel-side-container d-flex flex-direction-column">
                <div className="server-info-container">
                  {
                    this.state.home ? null : <ServerInfo dataName={this.state.dataServer} />
                  }
                </div>
                <div className="section-channel-side-content">
                  {
                    this.state.home ? <PrivateChannel/> : <ServerChannel dataServer={this.state.dataServer} channelClick={this.channelClick} />
                  }
                </div>
                <div className="user-info-container mt-auto">
                  <UserInfo openSettings={this.settingUser}/>
                </div>
              </section>
              <section className="section-chat-side-container">
                {
                  (!this.state.home) && this.state.channel.name ? <Chat socket={this.socket} dataServer={this.state.dataServer} chatMode={this.chatMode} channel={this.state.channel}/> : <Friends socket={this.socket} loadHome={this.loadHome} chatMode={this.chatMode}/>
                  // !this.state.home ? <Chat dataServer={this.state.dataServer} channelClick={this.state.channelClick}/> : <RelationShip/>
                }
              </section>
            </div> : null
        }
        {this.state.openSettings ? <UserSettings toggleClose={this.settingUser} /> : null}
      </div>
    );
  }
}

export default Home;
