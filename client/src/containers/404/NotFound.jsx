import React from 'react';

/**
 * Class NotFound
 */
class NotFound extends React.Component {
  /**
   * Render of class NotFound
   * @return {JSX.Element}
   */
  render() {
    return (
      <div>
        <h1>NotFound</h1>
      </div>
    );
  }
}

export default NotFound;
