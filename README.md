## /!\ CLONE OF DISCORD /!\

### Technologies used:
<ul>
    <li>ESlint</li>
    Technologies chat
    <ul>
        <li>express</li>
        <li>socket.io</li>
    </ul><br/>
    Technologies client
    <ul>
        <li>React</li>
        <li>React-Router</li>
        <li>i18next</li>
        <li>socket.io-client</li>
        <li>SCSS</li>
    </ul>
</ul>

### Installation Server Socket (Chat)
```
1 > git clone https://gitlab.com/baptiste.brand/friendisc.git
2 > cd friendisc
3 > npm install
```

### Installation Client React
```
(After installing the chat)
1 > cd client
2 > npm install
3 > npm start
```

### Preview
![login](./preview/login.png)
![register](./preview/register.png)
![friends_home](./preview/friends_home.png)
![chat](./preview/chat.png)
