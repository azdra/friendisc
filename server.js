const express = require('express');
const path = require('path');
const http = require('http');
const socket = require('socket.io');
const app = express();
const server = http.createServer(app);
const io = socket(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST'],
  },
});
const PORT = 4000;

server.listen(PORT, () => console.log('server is running on port %d', PORT));

const usersLogged = [];
const usersIdLogged = [];
const usersFriends = [];

// Routing
app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', (socket) => {
  // when the client emits 'message', this listens and executes
  socket.on('MESSAGE_SERVER_SEND', (data) => {
    // we tell the client to execute 'message'
    io.emit('MESSAGE_SERVER_RECEIVE', data);
  });

  socket.on('ADD_SERVER_CREATE_SEND', (data) => {
    io.to(socket.id).emit('ADD_SERVER_CREATE_RECEIVE', data);
  });

  socket.on('ADD_SERVER_JOIN_SEND', (data) => {
    io.to(socket.id).emit('ADD_SERVER_JOIN_RECEIVE', data);
  });

  socket.on('USER_LOGIN_SEND', (data) => {
    usersLogged[socket.id] = Object.assign(data, {
      logged: true,
      socketId: socket.id,
    });
    usersIdLogged[data.id] = socket.id;
    usersFriends[socket.id] = [];
    io.emit('USER_LOGIN_RECEIVE', data);
  });

  socket.on('GET_FRIENDS_LOGIN_LIST_SEND', (friends) => {
    const friendsList = [];
    for (const friend of friends) {
      usersFriends[socket.id].push(friend.id);

      if (usersIdLogged[friend.id]) {
        Object.assign(friend, {logged: true});
        friendsList.push(friend);
      } else {
        friendsList.push(friend);
      }
    }
    io.to(socket.id).emit('GET_FRIENDS_LOGIN_LIST_RECEIVE', friendsList);
    if (usersFriends[socket.id]) {
      for (const usersFriend of usersFriends[socket.id]) {
        io.to(usersIdLogged[usersFriend]).emit('FRIENDS_LIST_CONNECT_SEND', usersLogged[socket.id]);
      }
    }
  });

  // 6771066962530271232
  socket.on('FRIENDS_SEND_REQUEST_SEND', (data) => {
    const {friend} = data;
    if (usersIdLogged[friend.id]) {
      io.to(usersIdLogged[friend.id]).emit('FRIENDS_SEND_REQUEST_RECEIVE', usersLogged[socket.id]);
    }
    if (usersLogged[socket.id]) {
      if (usersIdLogged[friend.id]) {
        io.to(socket.id).emit('FRIENDS_SEND_REQUEST_RECEIVE', Object.assign(friend, {logged: true}));
      } else {
        io.to(socket.id).emit('FRIENDS_SEND_REQUEST_RECEIVE', friend);
      }
    }
  });

  socket.on('disconnect', (reason) => {
    if (usersLogged[socket.id]) {
      for (const usersFriend of usersFriends[socket.id]) {
        io.to(usersIdLogged[usersFriend]).emit('FRIENDS_LIST_DISCONNECT_SEND', usersLogged[socket.id]);
      }
      delete usersIdLogged[usersLogged[socket.id].id];
      delete usersLogged[socket.id];
      delete usersFriends[socket.id];
    }
  });
});
